from elasticsearch import Elasticsearch

es = Elasticsearch()

#es.index(index="my-index", doc_type="test-type", id=42, body={"any": "data", "timestamp": datetime.now()})
doc = {
    'author': 'Sudip',
    'text': 'Elasticsearch is cool',
}

res = es.index(index="test-index2", doc_type='tweet', id=1, body=doc)
print(res['created'])

res = es.get(index="test-index2", doc_type='tweet', id=1)
print(res['_source'])

