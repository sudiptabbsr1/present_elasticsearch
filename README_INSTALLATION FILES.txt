The Repo includes:

A presentation file.
A sample text files with JSON data to pass on the server and queries to run on server.
A sample Python file, to show the demo of "elasticsearch" package.


Required Installations:

//Installing the RESTClient API for firefox to interact with the server:
https://addons.mozilla.org/en-US/firefox/addon/restclient/

//Installing server to run the queries on localhost
-Elasticsearch server(.zip file) from: 
	http://www.elasticsearch.org/download/

//Installing package for running the "Elasticsearch.py"
-Installing py2neo package:
		pip install elasticsearch